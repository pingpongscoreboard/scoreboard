const deviceModule = require('../node_modules/aws-iot-device-sdk').device;
const cmdLineProcess = require('../lib/cmdline');
const awayTeam = require('../routes/homeTeam');
const Curl = require('node-libcurl').Curl;



function buttonPress(args) {
  //
   // The device module exports an MQTT instance, which will attempt
   // to connect to the AWS IoT endpoint configured in the arguments.
   // Once connected, it will emit events which our application can
   // handle.
   //
   const device = deviceModule({
      keyPath: args.privateKey,
      certPath: args.clientCert,
      caPath: args.caCert,
      clientId: args.clientId,
      region: args.region,
      baseReconnectTimeMs: args.baseReconnectTimeMs,
      keepalive: args.keepAlive,
      protocol: args.Protocol,
      port: args.Port,
      host: args.Host,
      debug: args.Debug
    });

   var curl = new Curl();
   curl.setOpt( 'FOLLOWLOCATION', true );

   curl.on( 'end', function( statusCode, body, headers ) {
    console.info( statusCode );
    console.info( '---' );
    console.info( body.length );
    console.info( '---' );
    console.info( this.getInfo( 'TOTAL_TIME' ) );

   });

   curl.on( 'error', function(error) {
          console.log('error', error);
        });


   var timeout;
   var count = 0;
   const minimumDelay = 250;
   var parsedMessage;

   device.subscribe('iotbutton/G030MD0401563HU1');

   if ((Math.max(args.delay, minimumDelay)) !== args.delay) {
      console.log('substituting ' + minimumDelay + 'ms delay for ' + args.delay + 'ms...');
   }

   device
      .on('connect', function() {
         console.log('d2 connect');
      });
   device
      .on('close', function() {
         console.log('d2 close');
      });
   device
      .on('reconnect', function() {
         console.log('d2 reconnect');
      });
   device
      .on('offline', function() {
         console.log('d2 offline');
      });
   device
      .on('error', function(error) {
         console.log('d2 error', error);
      });
   device
      .on('message', function(topic, payload) {
         console.log('message', topic, payload.toString());
         parsedMessage = JSON.parse(payload);
         console.log('message', parsedMessage.clickType);
         if(parsedMessage.clickType == "SINGLE") {
           curl.setOpt('URL', 'http://localhost:3000/homeTeam/increment');
           curl.perform();
         } else if(parsedMessage.clickType == "DOUBLE") {
           curl.setOpt('URL', 'http://localhost:3000/homeTeam/decrement');
           curl.perform();
         } else if(parsedMessage.clickType == "LONG") {
           curl.setOpt('URL', 'http://localhost:3000/reset');
           curl.perform();
         }
      });
}


module.exports = cmdLineProcess;

if (require.main === module) {
    cmdLineProcess('connect to the AWS IoT service and publish/subscribe to topics using MQTT for button two, test modes 1-2',
    process.argv.slice(2), buttonPress);
}
