var express = require('express');
var router = express.Router();

var scoreHome = 0;
var scoreAway = 0;

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', {
    title: 'Express',
    player1: 'left player',
    player2: 'right player',
    scoreHome: scoreHome,
    scoreAway: scoreAway,
  });
});

router.get('/reset', function(req, res, next) {
  res.io.emit('reset');
  res.send('team scores reset');
});

module.exports = router;
