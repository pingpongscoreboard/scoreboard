var express = require('express');
var router = express.Router();

/* GET users listing. */
router.get('/increment', function(req, res, next) {
  res.io.emit("homeTeamIncrement");
  res.send('score increased');
});

router.get('/decrement', function(req, res, next) {
  res.io.emit('homeTeamDecrement');
  res.send('score decreased');
});

module.exports = router;
