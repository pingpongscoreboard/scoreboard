var express = require('express');
var router = express.Router();

/* GET users listing. */
router.get('/increment', function (req, res, next) {
  res.io.emit('awayTeamIncrement');
  res.send('score increased');
});

router.get('/decrement', function(req, res, next) {
  res.io.emit('awayTeamDecrement');
  res.send('score decreased');
});

module.exports = router;
