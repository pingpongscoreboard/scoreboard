var socket = io('//localhost:3000');

socket.on('homeTeamIncrement', function (data) {
  var htmlElement = $('#homeTeam');
  var value = +htmlElement.text();
  value++;
  htmlElement.text(value);
});

socket.on('homeTeamDecrement', function (data) {
  var htmlElement = $('#homeTeam');
  var value = +htmlElement.text();
  if (value > 0) {
    value--;
  }
  htmlElement.text(value);
});

socket.on('awayTeamIncrement', function (data) {
  var htmlElement = $('#awayTeam');
  var value = +htmlElement.text();
  value++;
  htmlElement.text(value);
});

socket.on('awayTeamDecrement', function (data) {
  var htmlElement = $('#awayTeam');
  var value = +htmlElement.text();
  if (value > 0) {
    value--;
  }
  htmlElement.text(value);
});

socket.on('reset', function (data) {
  var awayElement = $('#awayTeam');
  awayElement.text(0);
  var homeElement = $('#homeTeam');
  homeElement.text(0);
});
