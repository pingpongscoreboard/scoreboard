# Ping Pong Scoreboard - IoT Office Hackathon

##Project Overview:
	Create a scoreboard UI to be displayed on the TV next to the ping pong table.  
	This scoreboard will be updated by ping pong players via Amazon IoT Buttons attached to each end of the table.

##Using the UI
 - run the ui with: node bin/www
 - to view the ui go to: http://localhost:3000
 - to update the score for the 'home team': curl http://localhost:3000/homeTeam/increment
 - to update the score for the 'away team': curl http://localhost:3000/awayTeam/increment

##Setting up button listeners
 - in separate terminal windows run
   ```node iot-button.js -f <certs directory> -H a3nk46o1xqsq4l.iot.us-west-2.amazonaws.com```
   ```node iot-button-two.js -f <certs directory> -H a3nk46o1xqsq4l.iot.us-west-2.amazonaws.com```
   certs directory: ~/deviceSDK/certs

##Stretch Goals:
	- HipChat integration - send game results to HipChat ping pong room
	- Player identification - display player name/ID on scoreboard
	- Store game results
	- Remotely hosted scoreboard endpoint

##Team Members:
	- Kim Brown - UI/UX Designer
	- Tyler Schuck - Developer
	- Paul Nguyen - Developer
	- Daniel Hamilton - Developer/Team Lead
	- Ted Crane - Advisor
